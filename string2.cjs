function string2(s) {
    if (typeof s!='string'){
        return []
    }
    var arr = s.split(".");
    if (arr.length !== 4){
        return []

    } 
    for (i=0;i<arr.length;i++) {
        if ( isNaN(arr[i]) || Number(arr[i]) < 0 || Number(arr[i]) > 255)
            return []
        arr[i]=Math.floor(arr[i])   
    }
    return arr
}
module.export=string2