function string4(obj){
    if (typeof obj != 'object'){
        return []

    }
    const arr=Object.values(obj)
    for (var i = 0; i < arr.length; i++) {
        arr[i]=arr[i].toLowerCase()
        arr[i] = arr[i].charAt(0).toUpperCase() + arr[i].slice(1);
    
    }
    
    let s=arr.join(" ")
    return s

}
module.export=string4